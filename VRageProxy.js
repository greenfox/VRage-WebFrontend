const express = require("express");
const VRageHMAC = require("./VRageHMAC");

module.exports = class VRageProxy {
  constructor(proxy, clients) {
    this.app = express();
    this.VRageHMAC = new VRageHMAC(proxy.key);

    for (let i of proxy.redirects) {
      this.createRedirect(i.from, clients[i.to]);
    }

    this.app.listen(proxy.port, () =>
      console.log("Proxy starting on port" + proxy.port + "!")
    );
  }
  /**
   *
   * @param {string} url
   * @param {VRageClient} VRageClient
   */
  createRedirect(url, VRageClient) {
    this.app.all(url + ":toURL(*)", (req, res) => {
      let requestStart = Date.now();
      addRequest();
      if (Object.keys(req.query).length != 0) {
        req.params.toURL = req.params.toURL + "?";
        for (let i in req.query) {
          req.params.toURL = req.params.toURL + i + "=" + req.query[i] + "&";
        }
        req.params.toURL = req.params.toURL.replace(/\$$/, "");

        //todo remove last char?
      }
      let toURL = req.params.toURL;
      let headers = req.headers;
      let actualAuth = headers.authorization;
      let expectedAuth = this.VRageHMAC.verifyAuth(
        req.originalUrl,
        headers.authorization,
        headers.date
      );
      if (actualAuth == expectedAuth) {
        VRageClient.request(toURL, req.method)
          .then(e => {
            console.log(
              "responce for: " +
                req.params.toURL +
                " durration: " +
                (Date.now() - requestStart) +
                "ms"
            );
            endRequest();
            res.json(e);
          })
          .catch(e => {
            console.log(
              "failed req  : " +
                req.params.toURL +
                " durration: " +
                (Date.now() - requestStart) +
                "ms"
            );
            console.log({
              clientError: e,
              headers: req.headers
            });
            res.status(500);
            res.send();
            ErrorPath(req.path);
          });
      } else {
        ErrorPath(req.path);
        res.json(401, { msg: "Invalid Auth" });
        console.log("Invalid Auth at " + req.originalUrl, errorRequests);
      }
    });
  }
};

let errorRequests = {};
function ErrorPath(path) {
  if (errorRequests[path] == null) {
    errorRequests[path] = 1;
  } else {
    errorRequests[path] = errorRequests[path] + 1;
  }
  console.log(errorRequests);
}

let echo = express();
echo.all("/*", (req, res) => {
  console.log(req.path, req.headers);
  res.send("hello world");
});
echo.listen(3000);

function addRequest() {
  activeRequests = activeRequests + 1;
  // console.log("Active Reqs:" + activeRequests);
}
function endRequest() {
  activeRequests = activeRequests - 1;
  // console.log("Active Reqs:" + activeRequests);
}
let activeRequests = 0;
