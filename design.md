# Open Screen

## SE Manager

|Hostname | Player Count | SimSpeed | PCU | Ping |
|---------|--------------|----------|-----|------|
|ZJ Server| 10/32        | 1.0      | 999 |   1  |
|{name}   | {players}    | {speed}  |{PCU}|{ping}|

Click on one of the above to shift to Main Screen.

# Main Screen

## Nav Bar:

Health, chat, blocks, players , factions

## Below content is controlled by the nav bar

### Health

Get graphs of player count, PCU use, CPU use, ping (hard to graph), 




# Config System

```js
//json
{
    vrageServers:[
        {name:"server1",url:"http://localhost:8080", key:"1234"},
        {name:"server2",url:"http://localhost:8081/this", key:"abc"},
        {name:"server3",url:"http://remoteHostname:8080", key:"someBase64Key"}
    ]
    proxy:{ //provides the exact same Remote VRageAPI via proxy
            //todo: ssl?
        port:9001,
        key:"A-base64-key",
        redirects:[ //these redirect to named vrage servers
            {from:"/main",to:"server1"},
            {from:"/backup",to:"server2"},
            {from:"/remote",to:"server3"}
        ]
    }
    web:{
        password:"asdf", //? idk about this one, maybe key would be better, but no one wants to type out all that...
        port:9080
    }
}
```