# VRage Web Toolkit

The intention of this project is to create a web toolkit for 
VRage servers (Space Engineers, possibly Medieval Engineers as well).

## VRage API Tool

A simple tool for querying VRage servers from NodeJS. This is the cornerstone
every feature here. (Status: beta)

## VRage Discord Bot 

A discord bot that allows querying server status and messaging. (Status: Alpha)

### Discord Bot Tutorial

- Install Node.js. https://nodejs.org/en/
- Create new folder, open it, shift right-click, `Open Command Window Here`
- In command window: `npm install https://gitlab.com/greenfox/VRage-WebFrontend.git`
- In command window: `"node_modules/.bin/VRageTookit" -d > config.yaml` (with quotes)
- Edit config.yaml with any text editor. You **need** to change the Key and webhook URL. The #comments explain other fields (multiple bots should work, but is untested)
- To start the app: `"node_modules/.bin/VRageTookit" -c config.yaml`, you can create a shortcut or add this to a run.bat file

Your config file can be yaml or json (any valid json is also valid yaml).

One app with config can run in multipule channels or with multipule VRage servers.

## Proxy Service

Allow chainable VRage Proxies servers. This will let you consolidate multiple
VRage WebAPIs into a single host. (Status: )

## Web VRage Client

Late stage goal. A web tool for managing grids, users, chat, etc. Also useful tool for simple monitoring tasks.

