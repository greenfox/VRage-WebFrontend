module.exports = {
    vrageServers:[
        {name:"mainServer",url:"http://localhost:8080", key:"mQLAdH7LKote83rhkH/zeQ=="},
        // {name:"serverLoopBack",url:"http://localhost:9001", path:"/main", key:"ckdKC3k1BJiqcvu7hdn3Jw=="},
        // {name:"testLoopback",url:"http://localhost:9001", path:"/loopback", key:"ckdKC3k1BJiqcvu7hdn3Jw=="},
    ],
    proxy:{ //provides the exact same Remote VRageAPI via proxy
            //todo: ssl?
        port:9001,
        key:"ckdKC3k1BJiqcvu7hdn3Jw==",
        redirects:[ //these redirect to named vrage servers
            //I'm definately violating data locality here...
            {from:"",to:"mainServer"}
            // {from:"/main",to:"mainServer"},
            // {from:"/loopback",to:"serverLoopBack"},
        ]
    },
    web:{
        password:"asdf", //? idk about this one, maybe key would be better, but no one wants to type out all that...
        port:9080
    }
}