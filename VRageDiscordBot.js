let Discord = require("discord.js");
let fetch = require("node-fetch");

class VRageDiscordBot {
  constructor(config) {
    this.name = config.Name;
    this.VRageServer = config.VRageServer;
    this.pollSecs = config.PollTiming * 1000; //probably need to make this a mutable thing /shrug
    this.webHook = null; //for now, this will be a webhook.
    this.serverUp = true;
    this.setUpWebHook(config);

    this.timeout = setInterval(() => {
      this.ping().then(e => {
        if (this.serverUp != e.up) {
          if (e.up) {
            this.webHook.send(this.VRageServer.name + ": Server is up!");
          } else {
            this.webHook.send(this.VRageServer.name + ": Server is down!");
          }
          this.serverUp = e.up;
        }
      });
    }, this.pollSecs);
  }
  ping() {
    return new Promise((res, rej) => {
      this.VRageServer.getPing()
        .then(e => {
          res({ up: true });
        })
        .catch(e => {
          res({ up: false });
        });
    });
  }
  setUpWebHook(config) {
    let WebHookURL = config.WebHookURL;
    return fetch(WebHookURL)
      .then(e => {
        return e.json();
      })
      .then(e => {
        this.webHook = new Discord.WebhookClient(e.id, e.token);
        console.log({
          name: this.name,
          status: "Webhook up"
        });
      })
      .catch(e => {
        console.error(config.Name + ": Webhook URL failed.");
        process.exit(-1);
      });
  }
  destory() {
    clearInterval(this.timeout);
  }
}
module.exports = VRageDiscordBot;
