#!/usr/bin/env node

let fs = require("fs");
let yaml = require("js-yaml");
let ArgumentParser = require("argparse").ArgumentParser;

let VRageClient = require("./VRageClient");
let VRageDiscordBot = require("./VRageDiscordBot");
let package = require("./package.json");

var parser = new ArgumentParser({
  version: package.version,
  addHelp: true,
  description: package.description
});
parser.addArgument(["-c", "--config"], {
  help: "config file to use, expected yaml or json"
  // required: true
});
parser.addArgument(["-d", "--discord"], {
  help: "discord bot example",
  action: "storeTrue"
});
var args = parser.parseArgs();
if (args.discord == true) {
  console.log('#Run with arguments like: "-c $DicordBotFile"');
  console.log("#Where $DicordBotFile is something like:");
  let a = fs.readFileSync(__dirname + "/ExampleDiscordBot.yaml", "utf8");
  console.log(a);
} else if (args.config) {
  var config = yaml.safeLoad(fs.readFileSync(args.config, "utf8"));

  //Set up VRage Clients
  let VRageClientList = {};
  for (let ClientConfig of config.VRageServers) {
    VRageClientList[ClientConfig.Name] = new VRageClient(ClientConfig);
  }
  let startTime = Date.now();
  for (let name in VRageClientList) {
    VRageClientList[name]
      .getPing()
      .then(e => {
        console.log({
          name: name,
          status: "Ping Responded!",
          queryTime: e.meta.queryTime + "ms",
          realTime: Date.now() - startTime + "ms"
        });
      })
      .catch(e => {
        if (e.error == "ERROR WITH HMAC!") {
          console.error(name + ": Key is likely wrong.");
        } else if (e.error.code == "ECONNREFUSED") {
          console.error(
            name +
              ": Connection refused, VRageAPI for web probably isn't running or network path is blocked."
          );
        }
      });
  }

  //Set up Discord Bots
  let DiscordBots = {};
  for (let DBotConfig of config.DiscordBots) {
    DBotConfig.VRageServer = VRageClientList[DBotConfig.VRageServer];
    DiscordBots[DBotConfig.Name] = new VRageDiscordBot(DBotConfig);
  }
} else {
  console.log("Run with -h for help.");
}
// debugger;
