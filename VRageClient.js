let fetch = require("node-fetch");

let VRageHMAC = require("./VRageHMAC");

const ln = "\r\n"; //Microsoft Newline

class VRageClient {
  constructor(config) {
    this.name = config.Name
    this.host = config.URL;
    this.path = config.path ? config.path : "";
    this.hmac = new VRageHMAC(config.Key);
  }
  getStatus() {
    return this.get("/vrageremote/v1/server");
  }
  getPing() {
    return this.get("/vrageremote/v1/server/ping");
  }
  getGridList() {
    return this.get("/vrageremote/v1/session/grids").then(e => {
      return e.data.Grids;
    });
  }
  request(requestPath, method) {
    let fullPath = this.path + requestPath;
    let headers = this.hmac.authForRequest(this.path + requestPath);
    return fetch(this.host + fullPath, {
      method: method,
      headers: headers
    })
      .then(e => {
        if(e.status == 403){
          throw "ERROR WITH HMAC!"
        }
        return e.json();
      })
      .catch(e => {
        throw {
          error: e,
          url: fullPath,
          method: method,
          headers: headers
        };
      });
  }
  get(url) {
    return this.request(url, "GET");
  }
}

module.exports = VRageClient;
