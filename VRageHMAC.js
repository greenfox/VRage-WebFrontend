const hmacsha1 = require("hmacsha1");
const atob = require("atob");

const ln = "\r\n"; //Microsoft Newline

//TODO: verify date is reasonable, don't accept old messages

module.exports = class VRageHMac {
  constructor(key) {
    this.key = atob(key);
  }
  authorization(url, nonce, date) {
    let msg = url + ln + nonce + ln + date + ln;
    let authorization = nonce + ":" + hmacsha1(this.key, msg);
    return authorization;
  }
  authForRequest(url) {
    let nonce = (Math.random() * 1000000000) | 0;
    let date = new Date().toUTCString();
    return {
      date: date,
      authorization: this.authorization(url, nonce, date)
    };
  }
  verifyAuth(url, auth, date) {
    let nonce = /^([0-9]+):/.exec(auth)[1];
    return this.authorization(url, nonce, date);
  }
};
